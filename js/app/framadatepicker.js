/**
 * This software is governed by the CeCILL-B license. If a copy of this license
 * is not distributed with this file, you can obtain one at
 * http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt
 *
 * Authors of STUdS (initial project): Guilhem BORGHESI (borghesi@unistra.fr) and Raphaël DROZ
 * Authors of Framadate/OpenSondage: Framasoft (https://github.com/framasoft)
 *
 * =============================
 *
 * Ce logiciel est régi par la licence CeCILL-B. Si une copie de cette licence
 * ne se trouve pas avec ce fichier vous pouvez l'obtenir sur
 * http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.txt
 *
 * Auteurs de STUdS (projet initial) : Guilhem BORGHESI (borghesi@unistra.fr) et Raphaël DROZ
 * Auteurs de Framadate/OpenSondage : Framasoft (https://github.com/framasoft)
 */
$(document).ready(function () {
    
    var init_datepicker = function () {
        // Default values are also set for dynamicaly added datepickers
        $.fn.datepicker.defaults.format =  window.date_formats.DATEPICKER || "dd/mm/yyyy";
        $.fn.datepicker.defaults.todayBtn = "linked";
        $.fn.datepicker.defaults.orientation = "bottom left";
        $.fn.datepicker.defaults.autoclose = true;
        $.fn.datepicker.defaults.language = lang;
        $.fn.datepicker.defaults.todayHighlight = true;
    };
    
    init_datepicker();

    $(document).on('click', '.input-group.date', function () {
        var startDate = '';
        var disabledDates = [];
        
        // if this is a "end range" datepicker : disable all dates before the "start range" datepicker
        if($(this).children('#range_end').length > 0){
            startDate = $(this).parents('.row').find('#range_start').val();
        }
        
        //already selected dates are no longer selectable
        $('.input-group.date input:visible').each(function(){
            if ($(this).val()){
                disabledDates.push($(this).val());
            }
        });

        $(this).datepicker({'container' : $(this).parent()}).datepicker('setDatesDisabled', disabledDates).datepicker('setStartDate', startDate).datepicker('show');
    });

    // Complete the date fields when use partialy fill it (eg: 15/01 could become 15/01/2016)

    var leftPad = function (text, pad) {
        return text ? pad.substring(0, pad.length - text.length) + text : text;
    };

    $(document).on('change', '.input-group.date input', function () {
        // Complete field if needed
        var val = $(this).val();
        var capture = /^([0-9]+)(?:\/([0-9]+))?$/.exec(val);

        if (capture) {
            var inputDay = leftPad(capture[1], "00"); // 5->05, 15->15
            var inputMonth = leftPad(capture[2], "00"); // 3->03, 11->11
            var inputDate = null;
            var now = new Date();

            if (inputMonth) {
                inputDate = new Date(now.getFullYear() + '-' + inputMonth + '-' + inputDay);

                // If new date is before now, add 1 year
                if (inputDate < now) {
                    inputDate.setFullYear(now.getFullYear() + 1);
                }
            } else {
                inputDate = new Date(now.getFullYear() + '-' + leftPad("" + (now.getMonth() + 1), "00") + '-' + inputDay);

                // If new date is before now, add 1 month
                if (inputDate < now) {
                    inputDate.setMonth(now.getMonth() + 1);
                }

            }

            $(this).val(inputDate.toLocaleFormat("%d/%m/%Y"));
        }
    });

});